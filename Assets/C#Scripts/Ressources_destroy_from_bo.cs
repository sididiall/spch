﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ressources_destroy_from_bo : MonoBehaviour {
	public float detroytime;

	// Use this for initialization
	void Start () {
		//Permet de detruire la trace du lazer en memoire pour ne pas surcharger le jeu
		Destroy(gameObject, detroytime);
	}

}
