﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieBoss_scripts : MonoBehaviour {
	Rigidbody enemieRigidBody;
	public Rigidbody laserMunitions;
	public Transform cannon1;
	public Transform cannon2;
	public Transform target;
	public Rigidbody laserLight;
	public float shootsRate;
	float nextShoot;
	public GameObject explosion;

	public int scoreValue;
	private GameController_script gameController;
	// Use this for initialization
	void Start () {
		enemieRigidBody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time>nextShoot)
		{
			//Gestion de la fréquence de tir
			nextShoot = Time.time + shootsRate;

			Rigidbody ShipsLightLeft;
			Rigidbody laserShipsLeft;

			//Instanciation cannon gauche
			laserShipsLeft = Instantiate (laserMunitions, cannon1.position, cannon1.rotation) as Rigidbody;
			laserShipsLeft.AddForce (cannon1.forward*1500);
			ShipsLightLeft = Instantiate (laserLight, cannon1.position, cannon1.rotation) as Rigidbody;
			ShipsLightLeft.AddForce (cannon1.forward*1500);

			Rigidbody ShipsLightRight;
			Rigidbody laserShipsRight;

			//Instanciation cannon droite
			laserShipsRight = Instantiate (laserMunitions, cannon2.position, cannon2.rotation) as Rigidbody;
			laserShipsRight.AddForce (cannon2.forward*1500);
			ShipsLightRight = Instantiate (laserLight, cannon2.position, cannon2.rotation) as Rigidbody;
			ShipsLightRight.AddForce (cannon2.forward*1500);
		}
	}
}
	
