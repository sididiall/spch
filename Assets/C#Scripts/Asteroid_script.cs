﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid_script : MonoBehaviour {

	Rigidbody asteroidBody;
	public float asteroidspeed;
	public Vector3 asteroidEulerVelocity;
	public GameObject explosion;

	public int scoreValue;
	private GameController_script gameController;

	// Update is called once per frame
	void Start () {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController_script>();
		}
		if (gameController == null)
		{
			//Classe contenenant des methodes pour facilité le deboggage

			Debug.Log ("Cannot find 'GameController' script");
		}

		asteroidBody = GetComponent<Rigidbody> ();
		asteroidBody.velocity = transform.forward *-1* asteroidspeed;
	}

	//Destruction des objets lors d'une collision
	void OnTriggerEnter(Collider other){
		if (other.tag != "Enemy") {
			Destroy (other.gameObject);
			Destroy (gameObject);
			Instantiate (explosion, transform.position, transform.rotation);
			gameController.AddScore(scoreValue);
		}

	}

	void FixedUpdate(){
		//Quaternion est utilisé pour les rotation
		Quaternion deltaRotation = Quaternion.Euler(asteroidEulerVelocity * Time.deltaTime);
		asteroidBody.MoveRotation (asteroidBody.rotation * deltaRotation);
	}
}
