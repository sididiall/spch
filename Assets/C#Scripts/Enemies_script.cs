﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies_script : MonoBehaviour {
	public float enemieSpeed;
	Rigidbody enemieRigidBody;
	public Rigidbody laserMunitions;
	public Transform enemieLeftCannon;
	public Transform enemieRightCannon;
	public Rigidbody laserLight;
	public float shootsRate;
	float nextShoot;
	public GameObject explosion;

	public int scoreValue;
	private GameController_script gameController;

	// Use this for initialization
	void Start () {
		//Permet de retourner un gameobject active par son étiquette sinnon retourne null s'il n'existe aucune référence
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController_script>();
		}
		if (gameController == null)
		{
			//Classe contenenant des methodes pour facilité le deboggage

			Debug.Log ("Cannot find 'GameController' script");
		}

		//Deplacement Ennemies
		enemieRigidBody = GetComponent<Rigidbody> ();
		enemieRigidBody.velocity = transform.forward*-1 *enemieSpeed;
	}

	//Destruction des objets lors d'une collision
	void OnTriggerEnter(Collider other){
		if (other.tag != "Enemy") {
			Destroy (other.gameObject);
			Destroy (gameObject);
			Instantiate (explosion, transform.position, transform.rotation);

			//appelle de la fonction de scoring
			gameController.AddScore(scoreValue);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time>nextShoot)
		{
			AudioSource audio = GetComponent<AudioSource> ();
			audio.Play ();
			//Gestion de la fréquence de tir
			nextShoot = Time.time + shootsRate;

			Rigidbody ShipsLightLeft;
			Rigidbody laserShipsLeft;

			//Instanciation cannon gauche
			laserShipsLeft = Instantiate (laserMunitions, enemieLeftCannon.position, enemieLeftCannon.rotation) as Rigidbody;
			laserShipsLeft.AddForce (enemieLeftCannon.forward*1500);
			ShipsLightLeft = Instantiate (laserLight, enemieLeftCannon.position, enemieLeftCannon.rotation) as Rigidbody;
			ShipsLightLeft.AddForce (enemieLeftCannon.forward*1500);

			Rigidbody ShipsLightRight;
			Rigidbody laserShipsRight;

			//Instanciation cannon droite
			laserShipsRight = Instantiate (laserMunitions, enemieRightCannon.position, enemieRightCannon.rotation) as Rigidbody;
			laserShipsRight.AddForce (enemieRightCannon.forward*1500);
			ShipsLightRight = Instantiate (laserLight, enemieRightCannon.position, enemieRightCannon.rotation) as Rigidbody;
			ShipsLightRight.AddForce (enemieRightCannon.forward*1500);
		}
	}
}
