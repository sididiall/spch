﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController_script : MonoBehaviour {
	public GameObject[] gameBlock;
	public Vector3 gamewavePosition;
	public int blockNumbers;
	public float gameWaveWait;
	public float gameWaveBegining;
	public float gameWaveInterval;
	public Canvas canvasGameOver;
	public Button restartBtn;
	public Button menuBtn;
	public Text gameOver;

	public GUIText scoresText;
	private int score;


	// Use this for initialization
	void Start () {

		//initialisation du score
		score = 0;
		UpdateScore ();

		//initialisation des Composants UI du GameOver
		canvasGameOver = canvasGameOver.GetComponent<Canvas> ();
		restartBtn = restartBtn.GetComponent<Button> ();
		menuBtn = menuBtn.GetComponent<Button> ();
		gameOver = gameOver.GetComponent<Text> ();

		//Couroutine est une unité de traitement qui s'apprente à une opération repetitive
		StartCoroutine(GameWaveAppear ());
	}

	public void Restart(){
	
		//Desactivation au lancement de la partie
		canvasGameOver.enabled = false;
		restartBtn.enabled = false;
		menuBtn.enabled = false;
		gameOver.enabled = false;

		//Application permet d'acceder au données d'execution
		//Permet de charcher le dernier niveau
		Application.LoadLevel(Application.loadedLevel);
	}
	public void BackToMenu(){
		
		canvasGameOver.enabled = false;
		restartBtn.enabled = false;
		menuBtn.enabled = false;
		gameOver.enabled = false;

		Application.LoadLevel("MenuDashboard");
	}
	
	// Update is called once per frame
	IEnumerator GameWaveAppear(){
		// yield -> mot clé qui indique q'un element est un itérateur
		yield return new WaitForSeconds(gameWaveBegining);
		while (true) {
			for (int i = 0; i < blockNumbers; i++) {
				Vector3 gameWave = new Vector3 (Random.Range (-gamewavePosition.x, gamewavePosition.x), gamewavePosition.y, gamewavePosition.z);
				Quaternion waveRotation = Quaternion.identity;
				Instantiate (gameBlock[Random.Range(0,5)], gameWave, waveRotation);
				yield return new WaitForSeconds(gameWaveWait);
			}
			yield return new WaitForSeconds (gameWaveInterval);
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore(){
		scoresText.text = "Score: " + score;
	}

}
