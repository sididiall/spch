﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background_Scrolling : MonoBehaviour {

	//variable de la vitesse de defilement du fond 
	public float scrollingSpeed;
	//variable permettant d'avoir acces à la texture et de le faire defiler
	Renderer rend;
	// Use this for initialization
	void Start () {
		// renvoie uniquement GameObjects actif.
		rend = gameObject.GetComponent<Renderer> ();
	}

	// Update is called once per frame
	void Update () {
		float offset = Time.time * scrollingSpeed;
		rend.material.mainTextureOffset = new Vector2 (offset, 0);
	}
}
