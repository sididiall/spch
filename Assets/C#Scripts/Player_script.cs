﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
	public float xMin,xMax,zMin,zMax;	
}
public class Player_script : MonoBehaviour {
	//Variable visible depuis l'interface	
	public float speed;
	public Boundary boundary;
	public float shipsRotation;
	public Rigidbody shipsLazer;
	public Transform cannon;
	public float shootSpeed;
	public Rigidbody lazerLight;
	public float shootsRate;
	float nextShoot;
	public Canvas canvasGameOver;
	public Button restartBtn;
	public Button menuBtn;
	public Text gameOver;


	void Start(){

		//initialisation des Composants UI du GameOver
		canvasGameOver = canvasGameOver.GetComponent<Canvas> ();
		restartBtn = restartBtn.GetComponent<Button> ();
		menuBtn = menuBtn.GetComponent<Button> ();
		gameOver = gameOver.GetComponent<Text> ();

		//Desactivation au lancement de la partie
		canvasGameOver.enabled = false;
		restartBtn.enabled = false;
		menuBtn.enabled = false;
		gameOver.enabled = false;

	}

	void OnDestroy(){		
		canvasGameOver.enabled = true;
		restartBtn.enabled = true;
		menuBtn.enabled = true;
		gameOver.enabled = true;
	}

	//Renvoie true pendant la trame pendant laquelle l'utilisateur a appuyé sur le bouton virtuel identifié par son nom
	void Update(){
		if(Input.GetButtonDown("Fire1") && Time.time>nextShoot)
		{
			AudioSource audio = GetComponent<AudioSource> ();
			audio.Play ();
			//Gestion de la fréquence de tir
			nextShoot = Time.time + shootsRate;

			Rigidbody laserMunitions;
			laserMunitions = Instantiate (shipsLazer,cannon.position,cannon.rotation) as Rigidbody;
			laserMunitions.AddForce (cannon.forward*shootSpeed);

			Rigidbody ShipsLight;
			ShipsLight = Instantiate (lazerLight,cannon.position,cannon.rotation) as Rigidbody;
			ShipsLight.AddForce (cannon.forward*shootSpeed);

		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		float HorizontalMove = Input.GetAxis("Vertical");
		float VerticalMove = Input.GetAxis("Horizontal");

		// Gere le mouvement du vaisseaux
		Vector3 move = new Vector3 (-HorizontalMove, 0, VerticalMove);

		//Gere la vitesse du vaisseau
		GetComponent<Rigidbody> ().velocity = move*speed;

		//GetComponent permet de saisir des données depuis l'interface utilisateur
		//Gere les limites du vaisseaux
		GetComponent<Rigidbody> ().position = new Vector3 (
			//Pinces des valeur entre une valeur float minimum et float maximum.
			Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			0,
			Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax));

		//Gere la rotation du vaissau
		GetComponent<Rigidbody> ().rotation = Quaternion.Euler(0f,0f,GetComponent<Rigidbody>().velocity.x*-shipsRotation);
	}
}
