﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth_Script : MonoBehaviour {

	private const int maxHealth = 100;
	public int currentHealth = maxHealth;
	public RectTransform healthbar;
	public GameObject explosion;

	public GUIText healthStatus;

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;
		if (currentHealth <= 0)
		{
			currentHealth = 0;
			Destroy (gameObject);
			Instantiate (explosion, transform.position, transform.rotation);
		}

		//Permet de mettre à jour l'affichage de la bar de santé pour refleter letat de santé
		healthbar.sizeDelta = new Vector2(currentHealth, healthbar.sizeDelta.y);
	}
}
