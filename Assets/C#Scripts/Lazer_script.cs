﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Lazer_script : MonoBehaviour {
	public GameObject explosion;

	// Use this for initialization

	void OnTriggerEnter(Collider other){
		var hit = other.gameObject;
		var health = hit.GetComponent<PlayerHealth_Script>();
		if (health  != null)
		{
			health.TakeDamage(10);
		}
	}
}
